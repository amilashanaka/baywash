-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2022 at 01:37 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bay_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `a_id` int(11) NOT NULL,
  `a_username` varchar(250) NOT NULL,
  `a_password` varchar(250) NOT NULL,
  `a_name` varchar(250) NOT NULL,
  `a_sec_key` varchar(250) DEFAULT NULL,
  `a_type` int(11) NOT NULL,
  `a_registered_by` int(11) DEFAULT NULL,
  `a_register_date` datetime NOT NULL DEFAULT current_timestamp(),
  `a_updated_by` int(11) DEFAULT NULL,
  `a_updated_date` datetime NOT NULL DEFAULT current_timestamp(),
  `a_type1_by` int(11) DEFAULT 0,
  `a_type2_by` int(11) DEFAULT 0,
  `a_type3_by` int(11) DEFAULT 0,
  `a_type4_by` int(11) DEFAULT 0,
  `a_type5_by` int(11) DEFAULT 0,
  `a_upline` int(11) DEFAULT 0,
  `a_currency` int(11) NOT NULL DEFAULT 0,
  `a_bank_name` varchar(255) DEFAULT NULL,
  `a_bank_account_no` varchar(255) DEFAULT NULL,
  `a_bank_branach` varchar(255) DEFAULT NULL,
  `a_address` varchar(255) DEFAULT NULL,
  `a_country` varchar(255) DEFAULT NULL,
  `a_city` varchar(255) DEFAULT NULL,
  `a_phone` varchar(255) DEFAULT NULL,
  `a_email` varchar(255) DEFAULT NULL,
  `a_state` varchar(255) DEFAULT NULL,
  `a_ref` int(11) DEFAULT 0,
  `a_location` varchar(20) DEFAULT NULL,
  `a_img` varchar(255) DEFAULT NULL,
  `a_lang` varchar(10) DEFAULT NULL,
  `a_last_log` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `a_status` int(11) NOT NULL DEFAULT 0,
  `bra_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`a_id`, `a_username`, `a_password`, `a_name`, `a_sec_key`, `a_type`, `a_registered_by`, `a_register_date`, `a_updated_by`, `a_updated_date`, `a_type1_by`, `a_type2_by`, `a_type3_by`, `a_type4_by`, `a_type5_by`, `a_upline`, `a_currency`, `a_bank_name`, `a_bank_account_no`, `a_bank_branach`, `a_address`, `a_country`, `a_city`, `a_phone`, `a_email`, `a_state`, `a_ref`, `a_location`, `a_img`, `a_lang`, `a_last_log`, `a_status`, `bra_id`) VALUES
(1, 'admin', '$2y$10$MCq3kqg5TpP5rvviemVayuO4Hvfxh3/JJ4mylf6IsX7rhT3gagTee', 'Admin', '5183', 1, 0, '2020-04-14 21:59:24', 0, '2020-04-15 02:56:23', 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'en', '2022-04-07 16:52:01', 1, 1),
(10, 'frontoffice', '$2y$10$iM5lsKCtb51O5Sqntc7qre/fISwgxwEsdcGaUDhtShMsPWaufoLdO', 'front office', '9619', 2, 1, '2021-07-17 00:00:00', NULL, '2021-07-17 05:52:19', 1, 0, 0, 0, 0, 1, 1, NULL, NULL, NULL, 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', 'Sri Lanka', 'Kottawa', '0777548249', 'dsa.amilashanaka@gmail.com', NULL, 7222, NULL, NULL, NULL, '2021-07-17 06:03:14', 1, 0),
(11, 'reshan', '$2y$10$bpW5h2N5Z1t4h6qWUhGdZupmYrarqJf1qrFK33S.qiaNuhOpL4cFe', 'Reshan Naurunna', '7382', 2, 1, '2021-07-21 00:00:00', NULL, '2021-07-21 18:49:48', 1, 0, 0, 0, 0, 1, 1, NULL, NULL, NULL, NULL, 'Malaysia', 'Petaling Jaya', NULL, NULL, NULL, 4921, NULL, NULL, NULL, '2022-02-15 10:17:26', 1, 0),
(12, 'mark', '$2y$10$1rSfkPuiQrVGYWB8S482r.E6R41.bZeS5sR5/wZNw5HvWGeg2XZFO', 'Mark', '5648', 2, 1, '2021-07-23 00:00:00', NULL, '2021-07-23 09:24:52', 1, 0, 0, 0, 0, 1, 1, NULL, NULL, NULL, NULL, 'Msia', 'PJ', NULL, NULL, NULL, 9247, NULL, NULL, NULL, '2022-02-15 10:27:40', 1, 0),
(13, 'michelle', '$2y$10$Q/5z7i71a032yl8rBLJELegdFfPDdGeHbMWAfu/EzwjJPQCtnL8tq', 'Michelle', '2570', 2, 1, '2021-07-23 00:00:00', NULL, '2021-07-23 09:25:26', 1, 0, 0, 0, 0, 1, 1, NULL, NULL, NULL, NULL, 'Msia', ' PJ', NULL, NULL, NULL, 5305, NULL, NULL, NULL, '2021-07-23 10:26:25', 1, 0),
(14, 'risberg', '$2y$10$0YATR2Ba0gc4UY.E4Q1L4ud/lqpE.gieW0HCyLA.01lc/eRa9mz2K', 'Risberg', '8619', 2, 1, '2021-07-23 00:00:00', NULL, '2021-07-23 09:25:51', 1, 0, 0, 0, 0, 1, 1, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, 5212, NULL, NULL, NULL, '2022-04-07 14:12:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_types`
--

CREATE TABLE `admin_types` (
  `at_id` int(11) NOT NULL,
  `at_name` varchar(100) NOT NULL,
  `at_level` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `admin_types`
--

INSERT INTO `admin_types` (`at_id`, `at_name`, `at_level`) VALUES
(1, 'supermaster', '1'),
(2, 'Admin', '2'),
(3, 'Distributor', '3'),
(4, 'Agency', '4'),
(5, 'Member', '5');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `bra_id` int(11) NOT NULL,
  `bra_name` varchar(255) DEFAULT NULL,
  `bra_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`bra_id`, `bra_name`, `bra_code`) VALUES
(1, 'TEST2', 'TEST');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `cu_id` int(11) NOT NULL,
  `cu_name` varchar(100) NOT NULL,
  `cu_rate` decimal(13,8) NOT NULL,
  `cu_withdraw_rate` decimal(13,8) NOT NULL,
  `cu_symbol` varchar(5) DEFAULT NULL,
  `cu_created_by` int(11) NOT NULL,
  `cu_created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `cu_updated_by` int(11) DEFAULT NULL,
  `cu_updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cu_status` int(11) NOT NULL DEFAULT 1,
  `cu_bank` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`cu_id`, `cu_name`, `cu_rate`, `cu_withdraw_rate`, `cu_symbol`, `cu_created_by`, `cu_created_date`, `cu_updated_by`, `cu_updated_date`, `cu_status`, `cu_bank`) VALUES
(1, 'USD ($)', '1.00000000', '1.00000000', '$', 1, '2020-04-16 00:00:00', 1, '2020-05-07 00:00:00', 1, '1.00000000'),
(2, 'TH', '32.46000000', '32.46000000', 'Ã Â¸Â', 1, '2020-04-19 12:51:55', 1, '2020-04-25 10:56:14', 1, '10');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `in_id` int(11) NOT NULL,
  `in_no` varchar(50) DEFAULT NULL,
  `u_id` int(11) DEFAULT 0,
  `v_id` int(11) DEFAULT 0,
  `pk_id` int(11) DEFAULT 0,
  `in_name` varchar(50) DEFAULT NULL,
  `in_contact` varchar(50) DEFAULT NULL,
  `in_address` varchar(255) NOT NULL,
  `in_desc` text DEFAULT NULL,
  `in_date` datetime DEFAULT NULL,
  `pay_type` varchar(20) DEFAULT NULL,
  `in_amount` decimal(13,8) DEFAULT 0.00000000,
  `in_status` int(11) DEFAULT 0,
  `in_created_by` int(11) DEFAULT 0,
  `in_created_dt` datetime DEFAULT NULL,
  `in_updated_by` int(11) DEFAULT 0,
  `in_updated_dt` datetime DEFAULT NULL,
  `bra_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`in_id`, `in_no`, `u_id`, `v_id`, `pk_id`, `in_name`, `in_contact`, `in_address`, `in_desc`, `in_date`, `pay_type`, `in_amount`, `in_status`, `in_created_by`, `in_created_dt`, `in_updated_by`, `in_updated_dt`, `bra_id`) VALUES
(1, NULL, 0, 0, 0, 'test', '35345', 'dsgdfgdf', '<p>dbdfbfd dfgfdg</p>', '2021-08-11 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-15 08:58:42', 0, NULL, 0),
(2, 'inv-2', 17, 0, 0, ' ', '12121211', 'tesrtt involvedv', '<p>test&nbsp;</p>', '2021-08-19 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-16 12:02:57', 0, NULL, 0),
(3, 'inv-3', 17, 0, 0, ' ', '8989789', 'kjhkjhjhuj', '<p>Test system&nbsp;</p>', '2021-08-21 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-18 18:28:44', 0, NULL, 0),
(4, 'inv-4', 17, 0, 0, ' ', '3454534', 'sfsdfsd', '<p>sfsdf</p>', '2021-08-21 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-18 18:30:14', 0, NULL, 0),
(5, 'inv-5', 17, 0, 0, ' ', '3454534', 'sfsdfsd', '<p>sfsdf</p>', '2021-08-21 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-18 18:30:14', 0, NULL, 0),
(6, 'inv-6', 10, 0, 0, ' ', '2452323', 'sfgsdfgdfsg', '<p>dgvdfgdf dfgdfgdfg&nbsp;</p>', '2021-08-20 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-18 21:42:35', 0, NULL, 0),
(7, 'inv-7', 17, 0, 0, ' ', '12121211', 'tesrtt involvedv', '', '2021-08-18 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-19 13:53:47', 0, NULL, 0),
(8, NULL, 0, 0, 0, '', NULL, '', NULL, NULL, NULL, '0.00000000', 0, 0, NULL, 0, NULL, 0),
(9, 'inv-9', 0, 0, 0, ' ', '12121211', 'tesrtt involvedv', '', '2021-08-20 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-19 13:57:52', 0, NULL, 0),
(10, NULL, 0, 0, 0, '', NULL, '', NULL, NULL, NULL, '0.00000000', 0, 0, NULL, 0, NULL, 0),
(11, NULL, 0, 0, 0, '', NULL, '', NULL, NULL, NULL, '0.00000000', 0, 0, NULL, 0, NULL, 0),
(12, NULL, 0, 0, 0, '', NULL, '', NULL, NULL, NULL, '0.00000000', 0, 0, NULL, 0, NULL, 0),
(13, NULL, 0, 0, 0, '', NULL, '', NULL, NULL, NULL, '0.00000000', 0, 0, NULL, 0, NULL, 0),
(14, 'inv-14', 18, 0, 0, ' ', '090909', 'test', '<p>fferfgter</p>', '2021-08-29 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 06:22:19', 0, NULL, 0),
(15, 'inv-15', 17, 0, 0, ' ', '4545656', 'ddddd', '<p>sfsdfdsf</p>', '2021-08-27 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 15:02:44', 0, NULL, 0),
(16, 'inv-16', 17, 0, 0, ' ', '0715478392', 'gfjkls', '<p>shkak</p>', '2021-08-29 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 16:53:40', 0, NULL, 0),
(17, 'inv-17', 17, 0, 0, ' ', '0986746785', 'GHJJKL', '<p>HJKJL<br></p>', '2021-08-03 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 17:11:54', 0, NULL, 0),
(18, 'inv-18', 17, 0, 0, ' ', '086785678', 'yhkghj', '<p>yuiyuu<br></p>', '2021-08-26 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 17:17:16', 0, NULL, 0),
(19, 'inv-19', 17, 0, 0, ' ', '0715478392', 'gfjkls', '<p>shkak</p>', '2021-08-29 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 16:53:40', 0, NULL, 0),
(20, 'inv-20', 16, 0, 0, ' ', '0715478392', 'gfjkls', '<p>RRRRR</p>', '2021-08-29 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 17:19:29', 0, NULL, 0),
(21, 'inv-21', 11, 0, 0, ' ', 'aas', 'ss', '', '2021-08-03 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 17:25:07', 0, NULL, 0),
(22, 'inv-22', 17, 0, 0, ' ', '0712348392232', 'ffff', '<p>wdewdefd<br></p>', '2021-08-27 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 17:35:58', 0, NULL, 0),
(23, 'inv-23', 17, 0, 0, ' ', '0715478392', 'gfjkls', '<p>eeeeeeeeeeeeeeeeee</p>', '2021-08-23 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 17:47:14', 0, NULL, 0),
(24, 'inv-24', 18, 0, 0, ' ', '021748549349', 'wweded', '<p>dewferf<br></p>', '2021-08-18 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-29 17:48:56', 0, NULL, 0),
(25, 'inv-25', 17, 0, 0, ' ', '983737', 'dssdd', '<p>gtgtt<br></p>', '2021-08-10 00:00:00', NULL, '0.00000000', 0, 1, '2021-08-30 15:05:21', 0, NULL, 0),
(26, 'inv-26', 10, 11, 0, ' ', '0715478392', 'no 257,kandy', '<p>aaaa</p>', '2021-08-31 00:00:00', 'Credit Card', '0.00000000', 0, 1, '2021-08-31 23:28:15', 0, NULL, 0),
(27, 'inv-27', 12, 11, 0, ' ', '99797453', 'fhfghgfhg', '<p>JIJIij 9jiihi&nbsp;</p>', '2021-09-08 00:00:00', 'Debit Card', '0.00000000', 0, 1, '2021-09-03 15:30:59', 0, NULL, 0),
(28, 'inv-28', 10, 11, 0, ' ', '541545', 'dgdfgdf', '<p>dfgfdgdf</p>', '2021-10-16 00:00:00', 'Cash', '0.00000000', 0, 1, '2021-10-27 21:28:15', 0, NULL, 0),
(29, 'inv-29', 13, 12, 0, ' ', '98989', 'jjijij', '<p>huhuygy</p>', '2021-11-24 00:00:00', 'Debit Card', '0.00000000', 0, 1, '2021-11-01 21:14:38', 0, NULL, 0),
(30, 'inv-30', 10, 11, 0, ' ', '5415454', 'dwedwedwe', '<p>qwdqwdqw</p>', '2021-11-19 00:00:00', 'Cash', '0.00000000', 0, 1, '2021-11-10 21:13:35', 0, NULL, 0),
(31, 'inv-31', 10, 12, 1, ' ', '65656956', 'sdxsdsd', '<p>sdsdsds</p>', '2021-11-13 00:00:00', 'Cash', '0.00000000', 0, 1, '2021-11-10 21:25:00', 0, NULL, 0),
(32, 'inv-32', 10, 11, 0, ' ', '01110986369', 'KL', '<p>test remark</p>', '2021-06-09 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-12 12:50:00', 0, NULL, 0),
(33, 'inv-33', 10, 11, 0, ' ', '01110986369', 'KL', '<p>dsdsdsd</p>', '2021-11-12 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-12 12:52:33', 0, NULL, 0),
(34, 'inv-34', 19, 11, 0, ' ', '01110986369', 'Heheheh', '<p>Test&nbsp;</p>', '2021-11-14 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-14 02:06:50', 0, NULL, 0),
(35, 'inv-35', 19, 11, 0, ' ', '01110986369', '', '<p>Test from kijal&nbsp;</p>', '2021-11-14 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-14 02:09:54', 0, NULL, 0),
(36, 'inv-36', 19, 11, 0, ' ', '01110986369', '', '<p>Test from kijal&nbsp;</p>', '2021-11-14 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-14 02:09:54', 0, NULL, 0),
(37, 'inv-37', 10, 11, 0, ' ', '01110986369', 'KL', '<p>test</p>', '2021-11-15 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-15 08:53:17', 0, NULL, 0),
(38, 'inv-38', 19, 11, 0, ' ', '01110986369', 'KL', '<p>test&nbsp;</p>', '2021-11-15 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-15 08:57:10', 0, NULL, 0),
(39, 'inv-39', 10, 11, 0, ' ', '124154221855421', 'gfdgdf', '<p>dfgdfgdfgdf</p>', '2021-11-15 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-15 15:37:39', 0, NULL, 0),
(40, 'inv-40', 10, 11, 3, ' ', '5675467547', '547457', '<p>tryuhrtfhrt</p>', '2021-11-16 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-15 15:43:29', 0, NULL, 0),
(41, 'inv-41', 11, 11, 3, ' ', '5675467547', '547457', '<p>tryuhrtfhrt</p>', '2021-11-16 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-15 15:43:29', 0, NULL, 0),
(42, 'inv-42', 10, 11, 1, ' ', '656546', 'fergferg', '<p>egferger<br></p>', '2021-11-17 00:00:00', 'Cash', '0.00000000', 0, 1, '2021-11-22 08:19:30', 0, NULL, 0),
(43, 'inv-43', 10, 11, 0, ' ', '01110986369', 'KL', '<p>tretre</p>', '2021-11-22 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-22 09:38:47', 0, NULL, 0),
(44, 'inv-44', 19, 11, 0, ' ', '01110986369', 'KL', '<p>test remark</p>', '2021-11-23 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-23 13:18:04', 0, NULL, 0),
(45, 'inv-45', 19, 9, 0, ' ', 'dsa.amilashanaka@gmail.com', 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', '', '2021-11-27 00:00:00', 'Cash', '0.00000000', 0, 1, '2021-11-24 08:16:45', 0, NULL, 0),
(46, 'inv-46', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', '', '', '2021-11-24 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-24 09:48:54', 0, NULL, 0),
(47, 'inv-47', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', '', '', '2021-11-24 00:00:00', 'Cash', '0.00000000', 0, 1, '2021-11-24 09:55:46', 0, NULL, 0),
(48, 'inv-48', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', '', '', '2021-11-24 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-24 09:59:00', 0, NULL, 0),
(49, 'inv-49', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', '', '', '2021-11-24 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-24 10:57:05', 0, NULL, 0),
(50, 'inv-50', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', '', '', '2021-11-25 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-25 10:05:55', 0, NULL, 0),
(51, 'inv-51', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', '', '<p>test</p>', '2021-11-25 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-25 11:04:57', 0, NULL, 0),
(52, 'inv-52', 22, 11, 0, ' ', 'dsa.amilashanaka@gmail.com', 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', '<p>test</p>', '2021-11-27 00:00:00', 'Cash', '0.00000000', 0, 1, '2021-11-26 06:10:07', 0, NULL, 0),
(53, 'inv-53', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', 'Kl', '<p>Test</p>', '2021-11-26 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-26 07:53:51', 0, NULL, 0),
(54, 'inv-54', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', 'dwswd', '<p>dsdsdsds</p>', '2021-11-26 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-26 10:24:59', 0, NULL, 0),
(55, 'inv-55', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', '', '<p>tets</p>', '2021-11-29 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-29 09:29:49', 0, NULL, 0),
(56, 'inv-56', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', 'KL', '<p>Remark</p>', '2021-11-29 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-11-29 09:46:02', 0, NULL, 0),
(57, 'inv-57', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', 'KL', '<p>ertert</p>', '2021-12-07 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-06 09:19:09', 0, NULL, 0),
(58, 'inv-58', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', 'dsfds', '<p>dsfsd</p>', '2021-12-06 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-06 09:20:29', 0, NULL, 0),
(59, 'inv-59', 19, 0, 2, ' ', '01110986369', 'KL', '<p>sdfdsfds</p>', '2021-12-06 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-06 09:24:10', 0, NULL, 0),
(60, 'inv-60', 19, 0, 2, ' ', 'rbhathiya@gmail.com', 'KL', '<p>test reshan 9.12.2021</p>', '2021-12-09 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-09 14:53:33', 0, NULL, 0),
(61, 'inv-61', 22, 11, 0, ' ', 'dsa.amiluiuiunaka@gmail.com', 'KL', '<p>dada</p>', '2021-12-10 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-10 09:31:14', 0, NULL, 0),
(62, 'inv-62', 0, 0, 8, ' ', 'dsa.amilashanaka@gmail.com', 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', '', '2021-12-18 00:00:00', 'Cash', '0.00000000', 0, 1, '2021-12-11 16:18:57', 0, NULL, 0),
(63, 'inv-63', 20, 0, 3, ' ', 'rbhathiya@gmail.com', 'KL', '<p>dsdsd</p>', '2021-12-12 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-12 12:33:55', 0, NULL, 0),
(64, 'inv-64', 15, 0, 4, ' ', 'rbhathtiy@gmail.com', 'jkl', '<p>test</p>', '2021-12-12 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-12 12:41:00', 0, NULL, 0),
(65, 'inv-65', 19, 0, 2, ' ', 'rbhathiya@gmail.com', 'KL', '<p>rwrwrw</p>', '2021-12-12 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-12 15:39:10', 0, NULL, 0),
(66, 'inv-66', 19, 0, 2, ' ', 'rbhathiya@gmail.com', 'KL', '<p>test</p>', '2021-12-13 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-13 14:55:30', 0, NULL, 0),
(67, 'inv-67', 19, 0, 2, ' ', 'rbhathiya@gmail.com', 'KL', '<p>test</p>', '2021-12-14 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-14 11:24:11', 0, NULL, 0),
(68, 'inv-68', 19, 0, 5, ' ', 'rbhatiy@gmail.com', 'rte', '<p>test</p>', '2021-12-14 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-14 11:25:47', 0, NULL, 0),
(69, 'inv-69', 19, 0, 5, ' ', '01110986369', 'KL', '<p>test</p>', '2021-12-19 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-19 10:44:47', 0, NULL, 0),
(70, 'inv-70', 19, 0, 5, ' ', '01110986369', 'KL', '<p>test 19.12.2021</p>', '2021-12-19 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-19 18:07:38', 0, NULL, 0),
(71, 'inv-71', 20, 0, 4, ' ', '01110986369', 'KL', '<p>testing 20.12.2021</p>', '2021-12-20 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-20 14:30:23', 0, NULL, 0),
(72, 'inv-72', 19, 0, 10, ' ', '01110986369', 'KL', '<p>test</p>', '2021-12-21 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-21 15:13:32', 0, NULL, 0),
(73, 'inv-73', 22, 11, 0, ' ', '1487529', 'KL', '<p>test</p>', '2021-12-21 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-21 18:15:12', 0, NULL, 0),
(74, 'inv-74', 21, 0, 3, ' ', '0163407054', 'KL', '', '2021-12-21 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-21 18:32:00', 0, NULL, 0),
(75, 'inv-75', 19, 0, 3, ' ', '01110986369', '', '', '2021-12-27 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-27 16:17:19', 0, NULL, 0),
(76, 'inv-76', 19, 0, 11, ' ', '01110986369', 'KL', '<p>sds</p>', '2021-12-27 00:00:00', 'Cash', '0.00000000', 0, 11, '2021-12-27 16:22:01', 0, NULL, 0),
(77, 'inv-77', 22, 11, 0, ' ', '1487529', 'kl', 'test', '2022-01-04 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-04 14:31:46', 0, NULL, 0),
(78, 'inv-78', 22, 11, 0, ' ', '1487529', '547457', '<p>uyt</p>', '2022-01-10 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-10 11:29:51', 0, NULL, 0),
(79, 'inv-79', 22, 11, 0, ' ', '1487529', 'kl', '<p>test</p>', '2022-01-11 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-01-11 18:55:28', 0, NULL, 0),
(80, 'inv-80', 22, 11, 0, ' ', 'dsa.amilashanaka@gmail.com', 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', '<p>sdsd</p>', '2022-01-11 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-01-11 18:58:45', 0, NULL, 0),
(81, 'inv-81', 19, 0, 11, ' ', '01110986369', 'KL', '<p>dfd</p>', '2022-01-11 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-11 19:28:06', 0, NULL, 0),
(82, 'inv-82', 22, 11, 0, ' ', '1487529', 'kl', '<p>test</p>', '2022-01-12 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-12 12:08:03', 0, NULL, 0),
(83, 'inv-83', 19, 0, 11, ' ', '01110986369', 'KL', '<p>new nnnnn</p>', '2022-01-12 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-12 15:29:46', 0, NULL, 0),
(84, 'inv-84', 22, 11, 0, ' ', '1487529', '', '<p>dsds</p>', '2022-01-21 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-21 09:36:39', 0, NULL, 0),
(85, 'inv-85', 22, 11, 0, ' ', '1487529', '', '', '2022-01-21 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-21 10:39:10', 0, NULL, 0),
(86, 'inv-86', 22, 11, 0, ' ', '1487529', 'KL', '', '2022-01-21 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-21 10:57:25', 0, NULL, 0),
(87, 'inv-87', 22, 11, 0, ' ', '1487529', 'KL', '<p>remark&nbsp;</p><p>sfsfsf</p><p>sf</p><p>s</p><p>f</p><p>s</p>', '2022-01-21 00:00:00', 'Credit Card', '0.00000000', 0, 11, '2022-01-21 10:58:00', 0, NULL, 0),
(88, 'inv-88', 22, 11, 0, ' ', '1487529', '', '<p>fdfdsf</p>', '2022-01-21 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-21 10:59:53', 0, NULL, 0),
(89, 'inv-89', 24, 0, 12, ' ', '1212123132', 'kl', '<p>gdsgsdgsd</p>', '2022-01-21 00:00:00', 'Debit Card', '0.00000000', 0, 11, '2022-01-21 11:07:55', 0, NULL, 0),
(90, 'inv-90', 22, 11, 0, ' ', '1487529', '', '', '2022-01-21 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-01-21 11:43:51', 0, NULL, 0),
(91, 'inv-91', 20, 0, 13, ' ', '01110986369', '', '<p>test</p>', '2022-02-09 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-02-09 11:14:21', 0, NULL, 0),
(92, 'inv-92', 22, 11, 0, ' ', '1487529', '', '<p>58</p>', '2022-02-10 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-02-09 11:20:40', 0, NULL, 0),
(93, 'inv-93', 22, 11, 0, ' ', '1487529', 'xc', '', '2022-02-10 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-10 23:00:55', 0, NULL, 0),
(94, 'inv-94', 17, 0, 14, ' ', '60179185798', '507 jalan seroja sungai koyan, 2, Sungai Koyan 3, 27650 Raub, Pahang, Malaysia', '', '2022-02-10 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-10 23:18:59', 0, NULL, 0),
(95, 'inv-95', 14, 11, 0, ' ', '601125449010', '34, Jalan U1, Taman Universiti, 35700Tanjong Malim, Perak, Malaysia', '<p>chk</p>', '2022-02-10 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-10 23:42:09', 0, NULL, 0),
(96, 'inv-96', 13, 10, 0, ' ', '6093401566', '22, Jalan U1, Taman Universiti, 35900 Tanjong Malim, Perak, Malaysia', '<p>chk invoice</p>', '2022-02-11 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-11 00:08:20', 0, NULL, 0),
(97, 'inv-97', 17, 0, 14, ' ', '60179185798', '507 jalan seroja sungai koyan, 2, Sungai Koyan 3, 27650 Raub, Pahang, Malaysia', '<p>chk package sale</p>', '2022-02-11 00:00:00', 'Credit Card', '0.00000000', 0, 1, '2022-02-11 00:38:39', 0, NULL, 0),
(98, 'inv-98', 14, 11, 0, ' ', '601125449010', '34, Jalan U1, Taman Universiti, 35700Tanjong Malim, Perak, Malaysia', '<p>bhjb</p>', '2022-02-11 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-11 02:09:47', 0, NULL, 0),
(99, 'inv-99', 21, 0, 15, ' ', '0163407054', 'KL', '<p>Test</p>', '2022-02-14 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-02-14 07:54:55', 0, NULL, 0),
(100, 'inv-100', 14, 11, 0, ' ', '601125449010', '34, Jalan U1, Taman Universiti, 35700Tanjong Malim, Perak, Malaysia', '', '2022-02-14 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-02-14 10:15:43', 0, NULL, 0),
(101, 'inv-101', 24, 0, 15, ' ', '1212123132', '', '', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 11, '2022-02-15 06:22:57', 0, NULL, 0),
(102, 'inv-102', 19, 0, 10, ' ', '01110986369', '', '<p>testing</p>', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 12, '2022-02-15 10:28:07', 0, NULL, 0),
(103, 'inv-103', 25, 16, 0, ' ', '1234567890', 'ds', '<p>dsds</p>', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 12, '2022-02-15 10:32:01', 0, NULL, 0),
(104, 'inv-104', 25, 0, 15, ' ', '1234567890', '', '', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 12, '2022-02-15 10:35:40', 0, NULL, 0),
(105, 'inv-105', 26, 17, 0, ' ', '234567876', '', '<p>sdfsdf</p>', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 12, '2022-02-15 11:14:25', 0, NULL, 0),
(106, 'inv-106', 26, 0, 15, ' ', '234567876', '', '', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 12, '2022-02-15 11:15:14', 0, NULL, 0),
(107, 'inv-107', 26, 0, 15, ' ', '234567876', 'KL', '<p>dsfds</p>', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 12, '2022-02-15 11:16:17', 0, NULL, 0),
(108, 'inv-108', 11, 0, 3, ' ', 'dsa.amilashanaka@gmail.com', 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', '<p>sale</p>', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-15 14:00:15', 0, NULL, 0),
(109, 'inv-109', 11, 0, 3, ' ', 'dsa.amilashanaka@gmail.com', 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', '<p>sale</p>', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-15 14:00:15', 0, NULL, 0),
(110, 'inv-110', 16, 0, 4, ' ', '60129056570', '', '', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-15 14:13:51', 0, NULL, 0),
(111, 'inv-111', 15, 0, 7, ' ', 'dsa.amilashanaka@gmail.com', 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', '', '2022-02-15 00:00:00', 'Credit Card', '0.00000000', 0, 1, '2022-02-15 14:25:00', 0, NULL, 0),
(112, 'inv-112', 15, 0, 7, ' ', 'dsa.amilashanaka@gmail.com', 'NO 11/46 , ranashinghe Mw , Hiripitiya , Panipitiya', '', '2022-02-15 00:00:00', 'Credit Card', '0.00000000', 0, 1, '2022-02-15 14:25:00', 0, NULL, 0),
(113, 'inv-113', 16, 0, 5, ' ', '60129056570', 'sss', '', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-15 15:06:27', 0, NULL, 0),
(114, 'inv-114', 16, 0, 5, ' ', 'dsa.amilashanaka@gmail.com', 'Overmhill Road', '', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-15 15:06:27', 0, NULL, 0),
(115, 'inv-115', 13, 9, 3, ' ', '6093401566', '22, Jalan U1, Taman Universiti, 35900 Tanjong Malim, Perak, Malaysia', '', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-15 15:52:47', 0, NULL, 0),
(116, 'inv-116', 13, 9, 3, ' ', '6093401566', '22, Jalan U1, Taman Universiti, 35900 Tanjong Malim, Perak, Malaysia', '', '2022-02-15 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-15 15:52:47', 0, NULL, 0),
(117, 'inv-117', 16, 10, 2, ' ', '60129056570', 'rr', '', '2022-02-16 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-16 20:20:40', 0, NULL, 0),
(118, 'inv-118', 16, 10, 2, ' ', '60129056570', '', '', '2022-02-16 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-02-16 20:20:40', 0, NULL, 0),
(119, 'inv-119', 16, 10, 2, ' ', '60129056570', '', '', '2022-03-15 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-03-15 19:54:12', 0, NULL, 0),
(120, 'inv-120', 13, 19, 0, ' ', '6093401566', '22, Jalan U1, Taman Universiti, 35900 Tanjong Malim, Perak, Malaysia', '', '2022-03-17 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-03-17 01:09:57', 0, NULL, 0),
(121, 'inv-121', 17, 11, 2, ' ', '60179185798', '507 jalan seroja sungai koyan, 2, Sungai Koyan 3, 27650 Raub, Pahang, Malaysia', '', '2022-03-17 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-03-17 01:38:04', 0, NULL, 0),
(122, 'inv-122', 14, 11, 0, ' ', '601125449010', '34, Jalan U1, Taman Universiti, 35700Tanjong Malim, Perak, Malaysia', '', '0000-00-00 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-04-01 00:22:53', 0, NULL, 0),
(123, 'inv-123', 24, 11, 6, ' ', 'asd', 'asd', '', '2022-03-31 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-04-01 00:23:39', 0, NULL, 0),
(124, 'inv-124', 13, 10, 18, ' ', '6093401566', '22, Jalan U1, Taman Universiti, 35900 Tanjong Malim, Perak, Malaysia', '', '2022-04-06 00:00:00', 'Cash', '0.00000000', 0, 1, '2022-04-06 02:34:53', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item`
--

CREATE TABLE `invoice_item` (
  `int_id` int(11) NOT NULL,
  `in_id` int(11) DEFAULT 0,
  `s_id` int(11) DEFAULT 0,
  `p_id` int(11) DEFAULT 0,
  `int_qty` int(11) DEFAULT 0,
  `int_amount` decimal(13,8) DEFAULT 0.00000000,
  `int_status` int(11) DEFAULT 0,
  `int_created_by` int(11) DEFAULT 0,
  `int_created_dt` datetime DEFAULT NULL,
  `int_updated_by` int(11) DEFAULT 0,
  `int_updated_dt` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

--
-- Dumping data for table `invoice_item`
--

INSERT INTO `invoice_item` (`int_id`, `in_id`, `s_id`, `p_id`, `int_qty`, `int_amount`, `int_status`, `int_created_by`, `int_created_dt`, `int_updated_by`, `int_updated_dt`) VALUES
(1, 1, 0, 1, 0, '0.00000000', 0, 0, NULL, 0, NULL),
(2, 4, 0, 4, 20, '10.00000000', 0, 0, NULL, 0, NULL),
(3, 4, 0, 4, 10, '10.00000000', 0, 0, NULL, 0, NULL),
(4, 4, 0, 4, 10, '10.00000000', 0, 0, NULL, 0, NULL),
(5, 6, 0, 4, 4, '10.00000000', 0, 0, NULL, 0, NULL),
(6, 6, 0, 4, 9, '10.00000000', 0, 0, NULL, 0, NULL),
(7, 7, 0, 4, 2, '10.00000000', 0, 0, NULL, 0, NULL),
(8, 9, 0, 4, 4, '10.00000000', 0, 0, NULL, 0, NULL),
(9, 14, 5, 0, 10, '24.56000000', 1, 0, NULL, 0, NULL),
(10, 14, 0, 4, 1, '10.00000000', 1, 0, NULL, 0, NULL),
(11, 15, 5, 0, 2, '24.56000000', 1, 0, NULL, 0, NULL),
(12, 15, 0, 4, 5, '10.00000000', 1, 0, NULL, 0, NULL),
(13, 16, 0, 4, 2, '10.00000000', 1, 0, NULL, 0, NULL),
(14, 16, 5, 0, 1, '24.56000000', 1, 0, NULL, 0, NULL),
(15, 18, 4, 0, 2, '10.01000000', 1, 0, NULL, 0, NULL),
(16, 16, 4, 0, 3, '10.01000000', 1, 0, NULL, 0, NULL),
(17, 18, 0, 4, 2, '10.00000000', 1, 0, NULL, 0, NULL),
(18, 18, 4, 0, 5, '10.01000000', 1, 0, NULL, 0, NULL),
(19, 23, 4, 0, 3, '10.01000000', 1, 0, NULL, 0, NULL),
(20, 23, 0, 4, 3, '10.00000000', 1, 0, NULL, 0, NULL),
(21, 24, 5, 0, 2, '24.56000000', 1, 0, NULL, 0, NULL),
(22, 24, 0, 4, 3, '10.00000000', 1, 0, NULL, 0, NULL),
(23, 25, 5, 0, 5, '24.56000000', 1, 0, NULL, 0, NULL),
(24, 25, 0, 4, 5, '10.00000000', 1, 0, NULL, 0, NULL),
(25, 26, 0, 4, 2, '10.00000000', 1, 0, NULL, 0, NULL),
(26, 26, 5, 0, 2, '24.56000000', 1, 0, NULL, 0, NULL),
(27, 27, 5, 0, 10, '24.56000000', 1, 0, NULL, 0, NULL),
(28, 27, 0, 4, 2, '10.00000000', 1, 0, NULL, 0, NULL),
(29, 28, 5, 0, 10, '24.56000000', 1, 0, NULL, 0, NULL),
(30, 28, 0, 4, 25, '10.00000000', 1, 0, NULL, 0, NULL),
(31, 29, 5, 0, 1, '24.56000000', 1, 0, NULL, 0, NULL),
(32, 29, 0, 4, 2, '10.00000000', 1, 0, NULL, 0, NULL),
(33, 30, 7, 0, 1, '25.00000000', 1, 0, NULL, 0, NULL),
(34, 30, 0, 4, 2, '10.00000000', 1, 0, NULL, 0, NULL),
(35, 32, 7, 0, 6, '25.00000000', 1, 0, NULL, 0, NULL),
(36, 32, 0, 4, 1, '10.00000000', 1, 0, NULL, 0, NULL),
(37, 33, 5, 0, 11, '24.56000000', 1, 0, NULL, 0, NULL),
(38, 33, 0, 4, 5, '10.00000000', 1, 0, NULL, 0, NULL),
(39, 34, 7, 0, 1, '25.00000000', 1, 0, NULL, 0, NULL),
(40, 34, 0, 4, 1, '10.00000000', 1, 0, NULL, 0, NULL),
(41, 36, 4, 0, 6, '10.01000000', 1, 0, NULL, 0, NULL),
(42, 36, 0, 4, 2, '10.00000000', 1, 0, NULL, 0, NULL),
(43, 37, 5, 0, 2, '24.56000000', 1, 0, NULL, 0, NULL),
(44, 37, 0, 4, 2, '10.00000000', 1, 0, NULL, 0, NULL),
(45, 38, 6, 0, 1, '67.00000000', 1, 0, NULL, 0, NULL),
(46, 38, 0, 5, 1, '50.00000000', 1, 0, NULL, 0, NULL),
(47, 39, 19, 0, 1, '42.45000000', 1, 0, NULL, 0, NULL),
(48, 39, 0, 5, 2, '50.00000000', 1, 0, NULL, 0, NULL),
(49, 39, 7, 0, 2, '34.42000000', 1, 0, NULL, 0, NULL),
(50, 43, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(51, 44, 52, 0, 2, '42.00000000', 1, 0, NULL, 0, NULL),
(52, 44, 0, 5, 2, '50.00000000', 1, 0, NULL, 0, NULL),
(53, 45, 18, 0, 1, '44.45000000', 1, 0, NULL, 0, NULL),
(54, 45, 0, 5, 2, '50.00000000', 1, 0, NULL, 0, NULL),
(55, 46, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(56, 46, 0, 5, 1, '50.00000000', 1, 0, NULL, 0, NULL),
(57, 47, 23, 0, 1, '47.06000000', 1, 0, NULL, 0, NULL),
(58, 50, 0, 5, 2, '50.00000000', 1, 0, NULL, 0, NULL),
(59, 50, 23, 0, 1, '47.06000000', 1, 0, NULL, 0, NULL),
(60, 51, 8, 0, 1, '14.62000000', 1, 0, NULL, 0, NULL),
(61, 51, 0, 4, 1, '33.90000000', 1, 0, NULL, 0, NULL),
(62, 52, 35, 0, 1, '810.08000000', 1, 0, NULL, 0, NULL),
(63, 53, 23, 0, 1, '47.06000000', 1, 0, NULL, 0, NULL),
(64, 53, 0, 5, 1, '29.00000000', 1, 0, NULL, 0, NULL),
(65, 54, 24, 0, 1, '47.26000000', 1, 0, NULL, 0, NULL),
(66, 54, 0, 6, 1, '23.00000000', 1, 0, NULL, 0, NULL),
(67, 55, 34, 0, 1, '186.04000000', 1, 0, NULL, 0, NULL),
(68, 55, 0, 6, 1, '23.00000000', 1, 0, NULL, 0, NULL),
(69, 56, 30, 0, 1, '1072.60000000', 1, 0, NULL, 0, NULL),
(70, 56, 0, 4, 1, '33.90000000', 1, 0, NULL, 0, NULL),
(71, 57, 18, 0, 1, '44.45000000', 1, 0, NULL, 0, NULL),
(72, 57, 0, 5, 1, '29.00000000', 1, 0, NULL, 0, NULL),
(73, 58, 23, 0, 1, '47.06000000', 1, 0, NULL, 0, NULL),
(74, 58, 0, 6, 1, '23.00000000', 1, 0, NULL, 0, NULL),
(75, 61, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(76, 61, 0, 5, 1, '29.00000000', 1, 0, NULL, 0, NULL),
(77, 78, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(78, 78, 0, 5, 1, '29.00000000', 1, 0, NULL, 0, NULL),
(79, 79, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(80, 79, 0, 6, 1, '23.00000000', 1, 0, NULL, 0, NULL),
(81, 80, 9, 0, 1, '22.02000000', 1, 0, NULL, 0, NULL),
(82, 80, 0, 5, 2, '29.00000000', 1, 0, NULL, 0, NULL),
(83, 82, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(84, 82, 0, 4, 1, '33.90000000', 1, 0, NULL, 0, NULL),
(85, 84, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(86, 84, 0, 7, 1, '23.90000000', 1, 0, NULL, 0, NULL),
(87, 85, 33, 0, 1, '176.06000000', 1, 0, NULL, 0, NULL),
(88, 85, 0, 6, 1, '23.00000000', 1, 0, NULL, 0, NULL),
(89, 87, 8, 0, 1, '14.62000000', 1, 0, NULL, 0, NULL),
(90, 87, 0, 6, 2, '23.00000000', 1, 0, NULL, 0, NULL),
(91, 88, 18, 0, 1, '44.45000000', 1, 0, NULL, 0, NULL),
(92, 90, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(93, 92, 18, 0, 1, '44.45000000', 1, 0, NULL, 0, NULL),
(94, 96, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(95, 96, 0, 5, 1, '29.00000000', 1, 0, NULL, 0, NULL),
(96, 100, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(97, 100, 0, 5, 1, '29.00000000', 1, 0, NULL, 0, NULL),
(98, 103, 18, 0, 1, '44.45000000', 1, 0, NULL, 0, NULL),
(99, 103, 0, 5, 1, '29.00000000', 1, 0, NULL, 0, NULL),
(100, 105, 8, 0, 1, '14.62000000', 1, 0, NULL, 0, NULL),
(101, 105, 0, 5, 1, '29.00000000', 1, 0, NULL, 0, NULL),
(102, 120, 32, 0, 1, '72.05000000', 1, 0, NULL, 0, NULL),
(103, 120, 0, 7, 1, '23.90000000', 1, 0, NULL, 0, NULL),
(104, 122, 18, 0, 0, '44.45000000', 1, 0, NULL, 0, NULL),
(105, 122, 0, 6, 10, '23.00000000', 1, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `pk_id` int(11) NOT NULL,
  `pk_code` varchar(50) DEFAULT NULL,
  `pk_name` varchar(50) DEFAULT NULL,
  `pk_img` varchar(255) NOT NULL,
  `pk_desc` text DEFAULT NULL,
  `pk_exp_date` datetime DEFAULT NULL,
  `pk_amount` decimal(13,8) DEFAULT 0.00000000,
  `pk_cost` decimal(13,8) DEFAULT 0.00000000,
  `pk_stock` decimal(13,8) DEFAULT 0.00000000,
  `pk_discount` decimal(13,8) DEFAULT 0.00000000,
  `pk_qty` int(11) NOT NULL,
  `pk_created_by` int(11) DEFAULT 0,
  `pk_created_dt` datetime DEFAULT NULL,
  `pk_updated_by` int(11) DEFAULT 0,
  `pk_updated_dt` datetime DEFAULT NULL,
  `pk_status` int(11) DEFAULT 0,
  `pk_used` int(11) DEFAULT 0,
  `pk_des` text DEFAULT NULL,
  `bra_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM AVG_ROW_LENGTH=122 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`pk_id`, `pk_code`, `pk_name`, `pk_img`, `pk_desc`, `pk_exp_date`, `pk_amount`, `pk_cost`, `pk_stock`, `pk_discount`, `pk_qty`, `pk_created_by`, `pk_created_dt`, `pk_updated_by`, `pk_updated_dt`, `pk_status`, `pk_used`, `pk_des`, `bra_id`) VALUES
(1, 'pkg-1', 'testpkg', '', '<p>jjhj</p>', '0000-00-00 00:00:00', '100.00000000', '70.00000000', '0.00000000', '30.00000000', 0, 1, '2021-11-10 21:24:08', 0, '2022-04-07 19:24:40', 0, 1, NULL, 1),
(2, 'pkg-2', 'tst11', '', '<p>test package</p>', '0000-00-00 00:00:00', '1000.00000000', '850.00000000', '0.00000000', '0.00000000', 79, 11, '2021-11-15 08:54:56', 0, '2022-04-06 02:31:52', 0, 1, NULL, 1),
(3, 'pkg-3', 'mark', '', '<p>dsgfedsgdsfghsd</p>', '2022-12-12 00:00:00', '2500.00000000', '1000.00000000', '0.00000000', '0.00000000', 41, 11, '2021-11-15 15:42:06', 0, NULL, 0, 1, NULL, 0),
(4, 'pkg-4', 'pkg-12', '../uploads/admin/packages/16375403311.jpeg', '<p>test <br></p>', '2021-11-23 00:00:00', '100.00000000', '90.00000000', '0.00000000', '10.00000000', 7, 1, '2021-11-22 08:18:23', 0, '2021-11-22 09:33:57', 1, 1, NULL, 0),
(5, 'pkg-5', 'package5', '', '', '2022-06-06 00:00:00', '500.00000000', '450.00000000', '0.00000000', '0.00000000', 43, 11, '2021-11-22 09:34:25', 0, NULL, 0, 1, NULL, 1),
(6, 'pkg-6', 'test pkg', '', '<p>test</p>', '2022-03-25 00:00:00', '1000.00000000', '500.00000000', '0.00000000', '0.00000000', 99, 11, '2021-11-24 11:00:14', 0, NULL, 1, 1, NULL, 0),
(7, 'pkg-7', 'DEMO ', '', '<p>DEMO PACKAGE</p>', '2023-05-18 00:00:00', '1500.00000000', '1000.00000000', '0.00000000', '0.00000000', 99, 11, '2021-11-29 09:59:28', 0, '2021-11-29 10:00:57', 1, 1, NULL, 0),
(8, 'pkg-8', 'DEMO PACKAGE', '', '<p>DEMO PACKAGE</p>', '2024-06-09 00:00:00', '2000.00000000', '1000.00000000', '0.00000000', '0.00000000', 96, 11, '2021-11-29 10:01:14', 0, NULL, 0, 1, NULL, 0),
(9, 'pkg-9', 'reshan pkg', '', '<p>reshan</p>', '2022-10-07 00:00:00', '1500.00000000', '0.00000000', '0.00000000', '0.00000000', 100, 11, '2021-12-14 11:30:41', 0, '2021-12-14 11:33:34', 0, 0, NULL, 0),
(10, 'pkg-10', 'Dr Mark', '', '<p>test remark</p>', '2022-11-24 00:00:00', '1500.00000000', '850.00000000', '0.00000000', '0.00000000', 98, 11, '2021-12-21 15:12:22', 0, NULL, 0, 1, NULL, 0),
(11, 'pkg-11', 'Faizal', '', '', '2025-10-27 00:00:00', '2500.00000000', '2000.00000000', '0.00000000', '0.00000000', 97, 11, '2021-12-27 16:20:30', 0, '2022-01-21 10:35:50', 0, 1, NULL, 0),
(12, 'pkg-12', 'Risberg_promotion', '', '', '2022-08-26 00:00:00', '1500.00000000', '1000.00000000', '0.00000000', '0.00000000', 4, 11, '2022-01-21 11:05:58', 0, NULL, 0, 1, NULL, 0),
(13, 'pkg-13', 'RESHAN FEB 2022', '', '<p>test</p>', '2023-08-17 00:00:00', '1500.00000000', '1000.00000000', '0.00000000', '0.00000000', 99, 11, '2022-02-09 11:13:01', 0, NULL, 0, 1, NULL, 0),
(14, 'pkg-14', 'pkg-20', '', '<p>sd</p>', '2022-02-17 00:00:00', '100.00000000', '80.00000000', '0.00000000', '20.00000000', 8, 1, '2022-02-10 23:13:41', 0, NULL, 1, 1, NULL, 0),
(15, 'pkg-15', 'abc', '', '<p>gfy</p>', '2022-02-12 00:00:00', '100.00000000', '90.00000000', '0.00000000', '20.00000000', 45, 1, '2022-02-11 06:26:34', 0, '2022-02-11 07:09:31', 0, 1, NULL, 0),
(16, 'pkg-16', 'MarchPromo', '', '<p>test remark march</p>', '2024-11-05 00:00:00', '6000.00000000', '4000.00000000', '0.00000000', '0.00000000', 6, 1, '2022-03-15 20:00:42', 0, NULL, 0, 0, NULL, 0),
(17, 'pkg-17', 'march promo', '', '<p>rere</p>', '2024-11-05 00:00:00', '6000.00000000', '4000.00000000', '0.00000000', '0.00000000', 6, 1, '2022-03-15 20:03:21', 0, NULL, 0, 0, NULL, 0),
(18, 'pkg-18', '1111111111', '', '', '2022-06-06 00:00:00', '100.00000000', '10.00000000', '0.00000000', '100.00000000', 999, 1, '2022-04-06 02:33:19', 0, NULL, 0, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `package_item`
--

CREATE TABLE `package_item` (
  `pkt_id` int(11) NOT NULL,
  `pk_id` int(11) DEFAULT 0,
  `s_id` int(11) DEFAULT 0,
  `p_id` int(11) DEFAULT 0,
  `int_qty` int(11) DEFAULT 0,
  `int_amount` int(11) DEFAULT 0,
  `pkt_status` int(11) DEFAULT 0,
  `pkt_created_by` int(11) DEFAULT 0,
  `pkt_created_dt` datetime DEFAULT NULL,
  `pkt_updated_by` int(11) DEFAULT 0,
  `pkt_updated_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `package_item`
--

INSERT INTO `package_item` (`pkt_id`, `pk_id`, `s_id`, `p_id`, `int_qty`, `int_amount`, `pkt_status`, `pkt_created_by`, `pkt_created_dt`, `pkt_updated_by`, `pkt_updated_dt`) VALUES
(3, 5, 7, 0, 10, 34, 1, 0, NULL, 0, NULL),
(4, 5, 0, 5, 5, 50, 1, 0, NULL, 0, NULL),
(5, 6, 4, 0, 1, 8, 1, 0, NULL, 0, NULL),
(6, 6, 0, 4, 2, 10, 1, 0, NULL, 0, NULL),
(7, 8, 4, 0, 5, 8, 1, 0, NULL, 0, NULL),
(8, 8, 0, 6, 1, 23, 1, 0, NULL, 0, NULL),
(9, 8, 8, 0, 2, 15, 1, 0, NULL, 0, NULL),
(10, 9, 4, 0, 10, 8, 1, 0, NULL, 0, NULL),
(11, 9, 0, 6, 3, 23, 1, 0, NULL, 0, NULL),
(12, 10, 8, 0, 2, 15, 1, 0, NULL, 0, NULL),
(13, 10, 0, 8, 5, 14, 1, 0, NULL, 0, NULL),
(14, 10, 4, 0, 10, 8, 1, 0, NULL, 0, NULL),
(15, 11, 4, 0, 10, 8, 1, 0, NULL, 0, NULL),
(16, 11, 8, 0, 2, 15, 1, 0, NULL, 0, NULL),
(17, 11, 18, 0, 1, 44, 1, 0, NULL, 0, NULL),
(18, 11, 0, 6, 1, 23, 1, 0, NULL, 0, NULL),
(19, 11, 0, 8, 2, 14, 1, 0, NULL, 0, NULL),
(20, 12, 4, 0, 10, 8, 1, 0, NULL, 0, NULL),
(21, 12, 0, 6, 2, 23, 1, 0, NULL, 0, NULL),
(22, 12, 18, 0, 2, 44, 1, 0, NULL, 0, NULL),
(23, 13, 4, 0, 10, 8, 1, 0, NULL, 0, NULL),
(24, 13, 7, 0, 2, 34, 1, 0, NULL, 0, NULL),
(25, 13, 0, 5, 2, 29, 1, 0, NULL, 0, NULL),
(26, 13, 19, 0, 2, 42, 1, 0, NULL, 0, NULL),
(27, 15, 4, 0, 2, 8, 1, 0, NULL, 0, NULL),
(28, 15, 0, 4, 7, 34, 1, 0, NULL, 0, NULL),
(29, 16, 6, 0, 1, 77, 1, 0, NULL, 0, NULL),
(30, 16, 0, 5, 2, 29, 1, 0, NULL, 0, NULL),
(31, 17, 4, 0, 10, 8, 1, 0, NULL, 0, NULL),
(32, 17, 0, 5, 5, 29, 1, 0, NULL, 0, NULL),
(33, 17, 8, 0, 10, 15, 1, 0, NULL, 0, NULL),
(34, 18, 5, 0, 5, 21, 1, 0, NULL, 0, NULL),
(35, 18, 0, 6, 5, 23, 1, 0, NULL, 0, NULL),
(36, 18, 0, 5, 5, 29, 1, 0, NULL, 0, NULL),
(37, 18, 9, 0, 5, 22, 1, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pkg_sold`
--

CREATE TABLE `pkg_sold` (
  `pks_id` int(11) NOT NULL,
  `pks_code` varchar(50) DEFAULT NULL,
  `u_id` varchar(50) DEFAULT NULL,
  `pks_exp_date` datetime DEFAULT NULL,
  `pks_created_by` int(11) DEFAULT 0,
  `pks_created_dt` datetime DEFAULT NULL,
  `pks_updated_by` int(11) DEFAULT 0,
  `pks_updated_dt` datetime DEFAULT NULL,
  `pks_status` int(11) DEFAULT 0,
  `pks_used` int(11) DEFAULT 0,
  `pk_id` int(11) DEFAULT 0,
  `bra_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM AVG_ROW_LENGTH=122 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `pkg_sold`
--

INSERT INTO `pkg_sold` (`pks_id`, `pks_code`, `u_id`, `pks_exp_date`, `pks_created_by`, `pks_created_dt`, `pks_updated_by`, `pks_updated_dt`, `pks_status`, `pks_used`, `pk_id`, `bra_id`) VALUES
(28, NULL, '16', NULL, 1, '2022-02-16 20:20:40', 0, NULL, 0, 0, 2, 0),
(29, NULL, '16', NULL, 1, '2022-02-16 20:20:40', 0, NULL, 0, 0, 2, 0),
(30, NULL, '13', NULL, 1, '2022-03-15 19:54:12', 0, NULL, 0, 0, 2, 0),
(31, NULL, '13', NULL, 1, '2022-03-17 01:09:57', 0, NULL, 0, 0, 1, 0),
(32, NULL, '13', NULL, 1, '2022-03-17 01:38:04', 0, NULL, 0, 0, 2, 0),
(33, NULL, '14', NULL, 1, '2022-04-01 00:22:53', 0, NULL, 0, 0, 0, 0),
(34, NULL, '24', NULL, 1, '2022-04-01 00:23:39', 0, NULL, 0, 0, 6, 0),
(35, NULL, '13', NULL, 1, '2022-04-06 02:34:53', 0, NULL, 0, 0, 18, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pkg_used`
--

CREATE TABLE `pkg_used` (
  `pu_id` int(11) NOT NULL,
  `pu_pkg_id` int(11) DEFAULT 0,
  `pu_v_id` int(11) DEFAULT 0,
  `pk_id` int(11) DEFAULT 0,
  `pu_status` int(11) DEFAULT 0,
  `pu_created_by` int(11) DEFAULT 0,
  `pu_created_dt` datetime DEFAULT NULL,
  `pu_updated_by` int(11) DEFAULT 0,
  `pu_updated_dt` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` int(11) NOT NULL,
  `p_code` varchar(50) DEFAULT NULL,
  `p_name` varchar(50) DEFAULT NULL,
  `p_img` varchar(255) NOT NULL,
  `p_desc` text DEFAULT NULL,
  `p_exp_date` datetime DEFAULT NULL,
  `p_amount` decimal(13,8) DEFAULT 0.00000000,
  `p_cost` decimal(13,8) DEFAULT 0.00000000,
  `p_stock` decimal(13,8) DEFAULT 0.00000000,
  `p_discount` decimal(13,8) DEFAULT 0.00000000,
  `p_created_by` int(11) DEFAULT 0,
  `p_created_dt` datetime DEFAULT NULL,
  `p_updated_by` int(11) DEFAULT 0,
  `p_updated_dt` datetime DEFAULT NULL,
  `p_status` int(11) DEFAULT 0,
  `bra_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM AVG_ROW_LENGTH=122 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `p_code`, `p_name`, `p_img`, `p_desc`, `p_exp_date`, `p_amount`, `p_cost`, `p_stock`, `p_discount`, `p_created_by`, `p_created_dt`, `p_updated_by`, `p_updated_dt`, `p_status`, `bra_id`) VALUES
(4, 'p-4', 'I phone cable', '../uploads/admin/products/16375344371.jpeg', '<p>I phone cable</p>', '2023-03-23 00:00:00', '33.90000000', '40.00000000', '100.00000000', '0.00000000', 1, '2021-08-18 18:29:38', 1, '2022-04-07 19:04:35', 0, 1),
(5, 'p-5', 'Android charging', '../uploads/admin/products/16375343781.jpeg', 'Android charging', '2023-05-25 00:00:00', '29.00000000', '40.00000000', '100.00000000', '0.00000000', 11, '2021-11-15 08:56:24', 11, '2021-11-25 10:10:03', 0, 0),
(6, 'p-6', 'Headphones', '', '<p>Headphones<br></p>', '2022-06-25 00:00:00', '23.00000000', '0.00000000', '100.00000000', '0.00000000', 11, '2021-11-25 10:08:21', 0, NULL, 0, 0),
(7, 'p-7', '3port 3.1A USB Car charger', '', '<p>3port 3.1A USB Car charger<br></p>', '2023-01-26 00:00:00', '23.90000000', '0.00000000', '100.00000000', '0.00000000', 11, '2021-11-25 10:08:54', 0, NULL, 0, 0),
(8, 'p-8', 'Perfume', '', '<p>Perfume<br></p>', '2023-03-25 00:00:00', '13.90000000', '0.00000000', '100.00000000', '0.00000000', 11, '2021-11-25 10:10:37', 0, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `s_id` int(11) NOT NULL,
  `s_code` varchar(50) DEFAULT NULL,
  `s_name` varchar(50) DEFAULT NULL,
  `s_img` varchar(255) DEFAULT NULL,
  `s_desc` text DEFAULT NULL,
  `s_amount` decimal(13,8) DEFAULT 0.00000000,
  `s_cost` decimal(13,8) DEFAULT 0.00000000,
  `s_discount` decimal(13,8) DEFAULT 0.00000000,
  `s_created_by` int(11) DEFAULT 0,
  `s_created_dt` datetime DEFAULT NULL,
  `s_updated_by` int(11) DEFAULT 0,
  `s_updated_dt` datetime DEFAULT NULL,
  `s_status` int(11) DEFAULT 0,
  `bra_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM AVG_ROW_LENGTH=122 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`s_id`, `s_code`, `s_name`, `s_img`, `s_desc`, `s_amount`, `s_cost`, `s_discount`, `s_created_by`, `s_created_dt`, `s_updated_by`, `s_updated_dt`, `s_status`, `bra_id`) VALUES
(4, NULL, 'QUICK WASH AND VACUUM', '../uploads/admin/services/16369620531.jpg', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"194\" style=\"width: 146pt;\"><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"194\" style=\"width: 146pt;\"><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" >  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"194\" style=\"width: 146pt;\"><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"194\" style=\"width: 146pt;\"><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" >15 min</td></tr></tbody></table>\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"194\" style=\"width: 146pt;\"><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"194\" style=\"width: 146pt;\"><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" >  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"194\" style=\"width: 146pt;\"><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"194\" style=\"width: 146pt;\"><tbody><tr height=\"17\" style=\"height:12.75pt\">\r\n  <td height=\"17\" class=\"xl65\" width=\"194\" style=\"height:12.75pt;width:146pt\">15 min</td></tr></tbody></table>', '8.01000000', '6.01000000', '0.00000000', 1, '2021-07-22 10:34:46', 1, '2022-04-07 19:09:08', 0, 1),
(5, NULL, 'PREMIUM WASH + VACUUM (includes multiwash)', '', '<p>30 min</p>', '20.82000000', '12.02000000', '0.00000000', 1, '2021-07-22 10:46:10', 11, '2021-11-15 14:53:56', 0, 0),
(6, NULL, 'HEADLIGHT RESTORATION + COATING', '', '<p>80 min<br></p>', '76.63000000', '60.00000000', '0.00000000', 1, '2021-08-29 17:23:25', 11, '2021-11-15 14:55:24', 0, 0),
(7, 'sev-7', 'HEADLIGHT RESTORATION', '', '', '34.42000000', '14.02000000', '0.00000000', 11, '2021-10-25 10:08:50', 11, '2021-11-15 14:54:41', 0, 0),
(8, 'sev-8', 'ENGINE DETAILING', '', '<p>55 min</p>', '14.62000000', '11.02000000', '0.00000000', 11, '2021-10-25 10:56:47', 11, '2021-11-15 14:56:35', 0, 0),
(9, 'sev-9', 'ENGINE DETAILING + COATING', '', '<p>60 min</p>', '22.02000000', '12.02000000', '0.00000000', 11, '2021-11-15 14:57:52', 0, NULL, 0, 0),
(10, 'sev-10', 'WINDSCREEN RESTORATION (FRONT)', '', '<p>75 min</p>', '39.62000000', '15.02000000', '0.00000000', 11, '2021-11-15 14:58:28', 0, NULL, 0, 0),
(11, 'sev-11', 'WINDSCREEN RESTORATION (FRONT + BACK)', '', '<p>105 min</p>', '91.27000000', '42.07000000', '0.00000000', 11, '2021-11-15 14:58:58', 0, NULL, 0, 0),
(12, 'sev-12', 'WINDSCREEN RESTORATION (ALL WINDOW)', '', '<p>135 min</p>', '128.09000000', '54.09000000', '0.00000000', 11, '2021-11-15 14:59:40', 0, NULL, 0, 0),
(13, 'sev-13', 'WINDSCREEN RESTORATION (FRONT) + COATING', '', '<p>105 min</p>', '59.63000000', '21.03000000', '0.00000000', 11, '2021-11-15 15:00:52', 0, NULL, 0, 0),
(14, 'sev-14', 'WINDSCREEN RESTORATION (FRONT + BACK) + COATING', '', '<p>135 min</p>', '131.29000000', '54.09000000', '0.00000000', 11, '2021-11-15 15:01:27', 0, NULL, 0, 0),
(15, 'sev-15', 'WINDSCREEN RESTORATION (ALL WINDOW) + COATING', '', '<p>180 min</p>', '187.72000000', '72.12000000', '0.00000000', 11, '2021-11-15 15:02:16', 0, NULL, 0, 0),
(16, 'sev-16', 'NANOMIST ADVANCE TECHNOLOGY', '', '<p>45 min</p>', '38.61000000', '9.01000000', '0.00000000', 11, '2021-11-15 15:05:06', 0, NULL, 0, 0),
(17, 'sev-17', 'INTERIOR UV CARE (Dashboard & Door Trim)', '', '<p>50 min</p>', '11.02000000', '10.02000000', '0.00000000', 11, '2021-11-15 15:05:43', 0, NULL, 0, 0),
(18, 'sev-18', 'CUSHION CLEANING', '', '<p>80 min</p>', '44.45000000', '32.05000000', '0.00000000', 11, '2021-11-15 15:06:37', 0, NULL, 0, 0),
(19, 'sev-19', 'LEATHER CLEANING', '', '<p>75 min</p>', '42.45000000', '30.05000000', '0.00000000', 11, '2021-11-15 15:07:25', 0, NULL, 0, 0),
(20, 'sev-20', 'LEATHER CONDITIONER & PROTECTANT (Leather Seats)', '', '<p>50 min</p>', '11.02000000', '10.02000000', '0.00000000', 11, '2021-11-15 15:08:10', 0, NULL, 0, 0),
(21, 'sev-21', 'NANO WAX (includes claying) (not including wash)', '', '<p>45 min</p>', '20.00000000', '15.00000000', '0.00000000', 11, '2021-11-15 15:09:17', 0, NULL, 0, 0),
(22, 'sev-22', 'SINGLE PROTECTION WAX', '', '<p>50 min</p>', '14.42000000', '10.02000000', '0.00000000', 11, '2021-11-15 15:10:36', 0, NULL, 0, 0),
(23, 'sev-23', 'DOUBLE PROTECTION WAX', '', '<p>90 min</p>', '47.06000000', '36.06000000', '0.00000000', 11, '2021-11-15 15:11:29', 0, NULL, 0, 0),
(24, 'sev-24', 'TRIPPLE PROTECTION WAX', '', '<p>90 min</p>', '47.26000000', '36.06000000', '0.00000000', 11, '2021-11-15 15:12:08', 0, NULL, 0, 0),
(25, 'sev-25', 'SEALANT', '', '<p>90 min</p>', '92.46000000', '36.06000000', '0.00000000', 11, '2021-11-15 15:12:43', 0, NULL, 0, 0),
(26, 'sev-26', 'POLISH & WAX (BASIC FINISHING) (1 Layer)', '', '<p>90 min</p>', '58.03000000', '18.03000000', '0.00000000', 11, '2021-11-15 15:13:16', 0, NULL, 0, 0),
(27, 'sev-27', 'POLISH & WAX (MEDIUM CUT) (3 Layer)', '', '<p>150 min</p>', '142.10000000', '60.10000000', '0.00000000', 11, '2021-11-15 15:13:44', 0, NULL, 0, 0),
(28, 'sev-28', 'POLISH & WAX (PERFECT CUT) (5 Layer)', '', '<p>270 min</p>', '249.77000000', '108.17000000', '0.00000000', 11, '2021-11-15 15:14:17', 0, NULL, 0, 0),
(29, 'sev-29', 'POLISH & WAX (DIAMOND CUT) (7 Layer) Mirror Effect', '', '<p>400 min</p>', '340.26000000', '160.26000000', '0.00000000', 11, '2021-11-15 15:14:58', 0, NULL, 0, 0),
(30, 'sev-30', 'PREMIUM WASH  PERFECT POLISH ', '', '<p>630 min</p>', '1072.60000000', '252.40000000', '0.00000000', 11, '2021-11-15 15:18:06', 0, NULL, 0, 0),
(31, 'sev-31', 'MAINTENANCE TOP COAT/CONDITIONER ', '', '<p>60 min<br><br>scheduled every 3&nbsp;<span style=\"font-size: 1rem;\">months</span></p>', '40.22000000', '12.02000000', '0.00000000', 11, '2021-11-15 15:18:42', 0, NULL, 0, 0),
(32, 'sev-32', 'Bumper', '', '<p>80 min</p>', '72.05000000', '32.05000000', '0.00000000', 11, '2021-11-15 15:19:51', 0, NULL, 0, 0),
(33, 'sev-33', 'Front Bonnet', '', '<p>90 min</p>', '176.06000000', '36.06000000', '0.00000000', 11, '2021-11-15 15:20:15', 0, NULL, 0, 0),
(34, 'sev-34', 'Front Fender', '', '<p>60 min</p>', '186.04000000', '24.04000000', '0.00000000', 11, '2021-11-15 15:20:42', 0, NULL, 0, 0),
(35, 'sev-35', 'Front Door', '', '<p>120 min</p>', '810.08000000', '48.08000000', '0.00000000', 11, '2021-11-15 15:21:13', 0, NULL, 0, 0),
(36, 'sev-36', 'Rear Door', '', '<p>120 min</p>', '1613.08000000', '48.08000000', '0.00000000', 11, '2021-11-15 15:22:40', 0, NULL, 0, 0),
(37, 'sev-37', 'Rear Fender', '', '<p>60 min</p>', '108.04000000', '24.04000000', '0.00000000', 11, '2021-11-15 15:23:31', 0, NULL, 0, 0),
(38, 'sev-38', 'Rear Boot', '', '<p>80 min</p>', '272.05000000', '32.05000000', '0.00000000', 11, '2021-11-15 15:23:56', 0, NULL, 0, 0),
(39, 'sev-39', 'Roof', '', '<p>90 min</p>', '967.06000000', '36.06000000', '0.00000000', 11, '2021-11-15 15:24:34', 0, NULL, 0, 0),
(40, 'sev-40', 'MOTORCYCLE QUICK WASH', '', '<p>10 min</p>', '3.60000000', '2.00000000', '0.00000000', 11, '2021-11-15 15:25:10', 0, NULL, 0, 0),
(41, 'sev-41', 'MOTORCYCLE DETAILED WASH', '', '<p>20 min</p>', '10.01000000', '4.01000000', '0.00000000', 11, '2021-11-15 15:25:34', 0, NULL, 0, 0),
(42, 'sev-42', 'MOTORCYCLE HEADLIGHT RESTORATION + COATING', '', '<p>35 min</p>', '32.21000000', '7.01000000', '0.00000000', 11, '2021-11-15 15:25:56', 0, NULL, 0, 0),
(43, 'sev-43', 'MOTORCYCLE QUICK WAX', '', '<p>30 min</p>', '7.81000000', '6.01000000', '0.00000000', 11, '2021-11-15 15:26:20', 0, NULL, 0, 0),
(44, 'sev-44', 'MOTORCYCLE COATING WAX / CARNAUBA WAX', '', '<p>40 min</p>', '15.01000000', '8.01000000', '0.00000000', 11, '2021-11-15 15:26:58', 0, NULL, 0, 0),
(45, 'sev-45', 'MOTORCYCLE BASIC POLISH & WAX', '', '<p>50 min</p>', '30.02000000', '10.02000000', '0.00000000', 11, '2021-11-15 15:27:24', 0, NULL, 0, 0),
(46, 'sev-46', 'MOTORCYCLE BODY COATING (9H)', '', '<p>120 min</p>', '84.04000000', '24.04000000', '0.00000000', 11, '2021-11-15 15:27:54', 0, NULL, 0, 0),
(47, 'sev-47', 'MOTORCYCLE COATING MAINTENANCE', '', '<p>30 min</p>', '18.41000000', '6.01000000', '0.00000000', 11, '2021-11-15 15:28:21', 0, NULL, 0, 0),
(48, 'sev-48', 'HELMET & VISOR WASH', '', '<p>5 min</p>', '1.80000000', '1.00000000', '0.00000000', 11, '2021-11-15 15:28:47', 0, NULL, 0, 0),
(49, 'sev-49', 'HELMET & VISOR QUICK WAX', '', '<p>15 min</p>', '3.90000000', '3.00000000', '0.00000000', 11, '2021-11-15 15:29:13', 0, NULL, 0, 0),
(50, 'sev-50', 'HELMET & VISOR COATING/CARNAUBA WAX', '', '<p>20 min</p>', '7.51000000', '4.01000000', '0.00000000', 11, '2021-11-15 15:29:45', 0, NULL, 0, 0),
(51, 'sev-51', 'HELMET & VISOR POLISH & WAX', '', '<p>25 min</p>', '15.01000000', '5.01000000', '0.00000000', 11, '2021-11-15 15:30:12', 0, NULL, 0, 0),
(52, 'sev-52', 'HELMET & VISOR COATING 9H', '', '<p>60 min</p>', '42.00000000', '12.02000000', '0.00000000', 11, '2021-11-15 15:30:32', 0, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `rt_id` int(11) NOT NULL,
  `rt_updater_by` int(11) NOT NULL,
  `rt_update_date` datetime NOT NULL DEFAULT current_timestamp(),
  `rt_a_L1` decimal(13,4) DEFAULT NULL,
  `rt_a_L2` decimal(13,4) DEFAULT NULL,
  `rt_a_L3` decimal(13,4) DEFAULT NULL,
  `rt_a_L4` decimal(13,4) DEFAULT NULL,
  `rt_a_L5` decimal(13,4) DEFAULT NULL,
  `rt_u_L1` decimal(13,4) DEFAULT NULL,
  `rt_u_L2` decimal(13,4) DEFAULT NULL,
  `rt_u_L3` decimal(13,4) DEFAULT NULL,
  `rt_u_L4` decimal(13,4) DEFAULT NULL,
  `rt_u_L5` decimal(13,4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`rt_id`, `rt_updater_by`, `rt_update_date`, `rt_a_L1`, `rt_a_L2`, `rt_a_L3`, `rt_a_L4`, `rt_a_L5`, `rt_u_L1`, `rt_u_L2`, `rt_u_L3`, `rt_u_L4`, `rt_u_L5`) VALUES
(1, 1, '2020-05-07 00:00:00', '0.0000', '6.0000', '9.0000', '7.0000', '0.0000', '3.0000', '4.0000', '5.0000', '6.0000', '50.0000');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `u_id` int(11) NOT NULL,
  `u_no` varchar(50) DEFAULT NULL,
  `u_username` varchar(250) DEFAULT NULL,
  `u_name` varchar(250) DEFAULT NULL,
  `u_password` varchar(250) DEFAULT NULL,
  `u_email` varchar(250) DEFAULT NULL,
  `u_otp` varchar(250) DEFAULT NULL,
  `u_phone` varchar(100) DEFAULT NULL,
  `u_dob` varchar(50) DEFAULT NULL,
  `u_bank_name` varchar(250) DEFAULT NULL,
  `u_bank_account_no` varchar(100) DEFAULT NULL,
  `u_bank_branch` varchar(250) DEFAULT NULL,
  `u_ic_no` varchar(255) DEFAULT NULL,
  `u_ic_type` varchar(255) DEFAULT NULL,
  `u_type` int(11) DEFAULT 0,
  `u_address` varchar(200) DEFAULT NULL,
  `u_country` varchar(255) DEFAULT NULL,
  `u_city` varchar(255) DEFAULT NULL,
  `u_outlet` varchar(255) DEFAULT NULL,
  `u_img` varchar(255) DEFAULT NULL,
  `u_lang` varchar(10) DEFAULT NULL,
  `u_last_log` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `u_currency` int(11) DEFAULT 0,
  `u_register_by` int(11) DEFAULT NULL,
  `u_register_date` datetime DEFAULT current_timestamp(),
  `u_updated_by` int(11) DEFAULT NULL,
  `u_updated_date` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `u_status` int(11) DEFAULT 1
) ENGINE=MyISAM AVG_ROW_LENGTH=162 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `u_no`, `u_username`, `u_name`, `u_password`, `u_email`, `u_otp`, `u_phone`, `u_dob`, `u_bank_name`, `u_bank_account_no`, `u_bank_branch`, `u_ic_no`, `u_ic_type`, `u_type`, `u_address`, `u_country`, `u_city`, `u_outlet`, `u_img`, `u_lang`, `u_last_log`, `u_currency`, `u_register_by`, `u_register_date`, `u_updated_by`, `u_updated_date`, `u_status`) VALUES
(25, NULL, '', 'Razik', '$2y$10$uBGWgYc4tYVSSxXeU2QtNuThZjRwuLXzgYpUW2oKRNztJwKLZgHsS', 'razik@yahoo.com', '3346', '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-15 10:29:47', 0, NULL, '2022-02-15 10:29:47', NULL, '2022-02-15 10:29:47', 1),
(11, NULL, 'Test', 'Ghhgghh', '$2y$10$d6kT2il1ORkYWPeT3AzUL.RMuBaGauUyakqmE3EcGlHhozsENMb9S', '', '1076', '', NULL, NULL, NULL, NULL, NULL, NULL, 2, '22, Jalan U1, Taman Universiti, 35900 Tanjong Malim, Perak, Malaysia', NULL, NULL, NULL, NULL, NULL, '2021-12-08 09:59:57', 0, NULL, '2021-07-19 14:27:34', NULL, '2021-12-08 09:59:57', 1),
(12, NULL, 'mary', 'mary snow', '$2y$10$xMCTnqUdPiJYIGTYPZMZqOfkAHG7W/cP.m3fNJg1Ga2PqjmKmIyuW', '', '4667', '', NULL, NULL, NULL, NULL, NULL, '1', 2, 'No,18, Jln Kinding Raya 16, Tmn KInding Raya, 31500 Tanjung Rambutan, Perak, Malaysia', NULL, NULL, NULL, NULL, NULL, '2021-07-21 15:50:42', 0, NULL, '2021-07-21 09:38:54', NULL, '2021-07-21 15:50:42', 1),
(13, NULL, '', 'john perera', '$2y$10$C1IcDEdu09Tw.J6vOzYZsuORL2TXIFKTe0ryQAP4Ds6fjmVPekHjC', 'john23@gmail.com', '1220', '6093401566', NULL, NULL, NULL, NULL, '930906-14-7043', 'IC', 2, '22, Jalan U1, Taman Universiti, 35900 Tanjong Malim, Perak, Malaysia', NULL, NULL, NULL, NULL, NULL, '2021-07-21 18:10:39', 0, NULL, '2021-07-21 17:48:48', NULL, '2021-07-21 18:10:39', 1),
(14, NULL, '', 'William James', '$2y$10$5aZ5DiWUOC0caQh4bzHCrOXoutBcFbx8./BF.ar7tVEWYj4wbHnr6', 'william67@gmail.com', '4680', '601125449010', NULL, NULL, NULL, NULL, '930906-14-7654', 'IC', 1, '34, Jalan U1, Taman Universiti, 35700Tanjong Malim, Perak, Malaysia', NULL, NULL, NULL, NULL, NULL, '2021-11-25 10:26:33', 0, NULL, '2021-07-21 18:19:55', NULL, '2021-11-25 10:26:33', 1),
(15, NULL, '', 'Jacob Logan', '$2y$10$WXe9kCl03RY2Rtle6hkjpuftIRCPA5j3tk92CR9rHmb4mQ1.CWjPa', 'jocob98@gmail.com', '7461', '6093401366', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 18:22:24', 0, NULL, '2021-07-21 18:22:24', NULL, '2021-07-21 18:22:24', 1),
(16, NULL, '', 'Jack Owen', '$2y$10$APp.zvaHsUCK/lKrmgYn7uZ7p/HaBgbLGnjEA72XruyxuRshE6yUW', 'jack567@gmail.com', '6790', '60129056570', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 18:25:01', 0, NULL, '2021-07-21 18:25:01', NULL, '2021-07-21 18:25:01', 1),
(17, NULL, '', 'David Grayson', '$2y$10$Dq31lwkBAKJMrYJY4nRaI.1LCyL3krDL9yVD3e8qlK7IxW8oUH.ia', 'devid324@gmail.com', '7121', '60179185798', NULL, NULL, NULL, NULL, '940906-15-7093', 'IC', 1, '507 jalan seroja sungai koyan, 2, Sungai Koyan 3, 27650 Raub, Pahang, Malaysia', NULL, NULL, NULL, NULL, NULL, '2021-07-21 18:26:33', 0, NULL, '2021-07-21 18:26:02', NULL, '2021-07-21 18:26:33', 1),
(24, NULL, '', 'Faizal', '$2y$10$eh.VFz/bceFBisZOlOLXF.YMuyIK8Ec5uEj2j4FZIB.dZXrFemfD6', 'faizal@13.com.my', '8225', '1212123132', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 11:07:46', 0, NULL, '2022-01-21 11:07:46', NULL, '2022-01-21 11:07:46', 1),
(19, NULL, '', 'Reshan', '$2y$10$YqsynLj0Yh5HHx/4ghDiL.ztIxi56yr6DLNI6Q7U3MGc3sZ.Rj1Aq', 'rbhathiya@gmail.com', '2170', '01110986369', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-22 00:00:39', 0, NULL, '2021-07-22 00:00:39', NULL, '2021-07-22 00:00:39', 1),
(20, NULL, '', 'Reshan N', '$2y$10$VnwG5/MyYLBJwscsZW9ipe35gBgVZG5ZhfK/lNMNS4ZVe2vppXVLa', 'rbhathiya@gmail.com', '5938', '01110986369', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-25 10:23:25', 0, NULL, '2021-07-22 00:30:40', NULL, '2021-11-25 10:23:25', 0),
(21, NULL, '', 'rn', '$2y$10$uSeo.2Y0PedKRRPM1IfRuOef9nPoxoOQ34vMZWQsSW4levdIkv6NO', 'rbhathiya@gmail.com', '6534', '0163407054', NULL, NULL, NULL, NULL, 'N2447631', 'Passport', 1, 'KL', NULL, NULL, NULL, NULL, NULL, '2021-08-05 23:47:04', 0, NULL, '2021-08-05 23:46:41', NULL, '2021-08-05 23:47:04', 1),
(22, NULL, '', 'testmember', '$2y$10$eMJbhgJf5/lMttyKYq4cy.pLG48z3M8VjzsM7TewJrtwV1bXb9S0a', 'dsa.amiluiuiunaka@gmail.com', '7777', '1487529', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-10 21:09:56', 0, NULL, '2021-11-10 21:09:56', NULL, '2021-11-10 21:09:56', 1),
(23, NULL, '', 'jhon', '$2y$10$PRRRMkEitxc9iez89JZ0ReRsSxlaHJsRUccivYiNBCa66mkzipO1e', 'jhon@jhon.com', '5612', '0165484975', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-10 21:17:12', 0, NULL, '2021-11-10 21:17:12', NULL, '2021-11-10 21:17:12', 1),
(26, NULL, '', 'Sunil', '$2y$10$vhRQohuv8P3h6ATJmk4.l.c46FW6dBX3M1mGDw06Ui4Zn42wU5bVi', 'sunil@yahoo.com', '7323', '234567876', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-15 11:13:16', 0, NULL, '2022-02-15 11:13:16', NULL, '2022-02-15 11:13:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `v_id` int(11) NOT NULL,
  `v_number` varchar(50) DEFAULT NULL,
  `v_type` varchar(50) DEFAULT NULL,
  `v_model` varchar(255) DEFAULT NULL,
  `v_mileage` double DEFAULT 0,
  `v_make` varchar(255) DEFAULT NULL,
  `v_img` varchar(255) DEFAULT NULL,
  `v_desc` text DEFAULT NULL,
  `v_owner` int(11) DEFAULT 0,
  `v_size` int(11) DEFAULT 0,
  `v_created_by` int(11) DEFAULT 0,
  `v_created_dt` datetime DEFAULT NULL,
  `v_updated_by` int(11) DEFAULT 0,
  `v_updated_dt` datetime DEFAULT NULL,
  `v_status` int(11) DEFAULT 0,
  `v_pkg` int(11) DEFAULT 0
) ENGINE=MyISAM AVG_ROW_LENGTH=122 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`v_id`, `v_number`, `v_type`, `v_model`, `v_mileage`, `v_make`, `v_img`, `v_desc`, `v_owner`, `v_size`, `v_created_by`, `v_created_dt`, `v_updated_by`, `v_updated_dt`, `v_status`, `v_pkg`) VALUES
(11, 'CCX 8228', 'Medium', 'SWIFT', 0, 'ZUSUKI', '../uploads/admin/vehicles/16283759171.jpg', '<p>Blue</p>', 14, 0, 11, '2021-07-22 00:00:47', 1, '2022-02-10 23:40:34', 0, 6),
(10, 'WER-3593', 'Medium', 'Land Cruiser', 0, 'Toyota', '../uploads/admin/vehicles/16283758491.jpg', '<p>this car is very good</p>', 13, 0, 1, '2021-07-21 18:26:52', 1, '2021-08-09 06:41:35', 0, 18),
(9, 'WVX-3589', 'Small', 'Prius', 0, 'Toyota', '../uploads/admin/vehicles/16283760241.jpg', '<p>This car has good condition</p>', 19, 0, 1, '2021-07-20 22:55:18', 1, '2021-08-09 06:50:36', 0, 3),
(12, 'kw-8811', 'Medium', 'prius', 0, 'toyota', '', '', 19, 0, 1, '2021-08-19 14:15:39', 11, '2021-12-14 11:38:22', 0, 3),
(13, 'tgt', 'Small', '', 0, 'toyota', '', 'wsd', 0, 0, 1, '2021-08-29 17:27:21', 0, NULL, 0, 0),
(14, 'CAB7428', 'Medium', 'prius', 230, 'Toyota', '', '<p>aaa</p>', 10, 0, 1, '2021-08-31 23:27:04', 0, NULL, 0, 0),
(15, 'vx5678', 'Medium', 'corolla', 123456, 'Toyota', '', 'note here&nbsp;', 23, 0, 1, '2021-11-10 21:17:31', 0, NULL, 0, 0),
(16, 'RAZ8456', 'Medium', 'COROLLA', 25000, 'TOYOTA', '', '', 25, 0, 12, '2022-02-15 10:30:12', 0, NULL, 0, 0),
(17, 'SUN4512', 'Large', 'HAHAHA', 150000, 'BMW', '', '', 26, 0, 12, '2022-02-15 11:13:23', 0, NULL, 0, 0),
(18, 'RES12345', 'Large', 'RRR', 12345, 'QQQQ', '', '', 19, 0, 12, '2022-02-15 11:41:51', 0, NULL, 0, 0),
(19, 'WER-3593', 'Small', '23456', 23456, '123456', '', '', 13, 0, 1, '2022-03-17 01:08:24', 0, NULL, 0, 4),
(20, 'WER-3593', 'Small', 'qwe', 234334, 'qwer', '', '', 13, 0, 1, '2022-03-17 01:09:09', 0, NULL, 0, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`a_id`) USING BTREE;

--
-- Indexes for table `admin_types`
--
ALTER TABLE `admin_types`
  ADD PRIMARY KEY (`at_id`) USING BTREE;

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`bra_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`cu_id`) USING BTREE;

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`in_id`) USING BTREE;

--
-- Indexes for table `invoice_item`
--
ALTER TABLE `invoice_item`
  ADD PRIMARY KEY (`int_id`) USING BTREE;

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`pk_id`) USING BTREE;

--
-- Indexes for table `package_item`
--
ALTER TABLE `package_item`
  ADD PRIMARY KEY (`pkt_id`) USING BTREE;

--
-- Indexes for table `pkg_sold`
--
ALTER TABLE `pkg_sold`
  ADD PRIMARY KEY (`pks_id`) USING BTREE;

--
-- Indexes for table `pkg_used`
--
ALTER TABLE `pkg_used`
  ADD PRIMARY KEY (`pu_id`) USING BTREE;

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`p_id`) USING BTREE;

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`s_id`) USING BTREE;

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`rt_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`u_id`) USING BTREE;

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`v_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `admin_types`
--
ALTER TABLE `admin_types`
  MODIFY `at_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `bra_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `cu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `in_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `invoice_item`
--
ALTER TABLE `invoice_item`
  MODIFY `int_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `pk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `package_item`
--
ALTER TABLE `package_item`
  MODIFY `pkt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `pkg_sold`
--
ALTER TABLE `pkg_sold`
  MODIFY `pks_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `pkg_used`
--
ALTER TABLE `pkg_used`
  MODIFY `pu_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `rt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `v_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
