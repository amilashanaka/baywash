<?php
include_once './top_header.php';
include_once './data/data_branch_list.php';
?>
<body class="hold-transition sidebar-mini">
    <?php
    if (isset($_GET['error'])) {
        $error = base64_decode($_GET['error']);
        echo '<script>  error_by_code(' . $error . ');</script>';
    }
    ?>    
    <div class="wrapper">
        <!-- Navbar -->
        <?php include_once './navbar.php'; ?>
        <?php include_once './sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->

            <?php
            $t1 = "Branch";
            $t2 = " List";

            include_once './page_header.php';
            ?>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="data/register_branch.php" class="form-horizontal" method="post" enctype="multipart/form-data" name="update_members" >

                                    <?php
                                    if ($bra_id == 0) {
                                        echo '<input type="hidden" name="action" value="register">';
                                    } else {
                                        echo ' <input type="hidden" name="action" value="update">';
                                        echo ' <input type="hidden" name="bra_id" value="' . $bra_id . '">';
                                    }
                                    ?>
                                    <div class="form-group row">
                                        <label for="tra_amount" class="col-sm-2 col-form-label">Branch Name</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" id="operation" name="bra_name" value="<?= $row['bra_name']; ?>">
                                        </div>
                                        <label for="hp_tra_amount" class="col-sm-2 col-form-label text-center">Branch Code</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" id="operation" name="bra_code" value="<?= $row['bra_code']; ?>">
                                        </div>
                                    </div>

                                    <div  class="col-lg-12 col-md-12 form-group ">
                                        <div class="col-lg-6 col-md-6 form-group" >
                                            <br>
                                        </div>
                                        <div class="col-lg-6 col-md-6 form-group ">
                                            <div class="row">
                                                <?php if ($bra_id == 0) { ?>

                                                    <div class="col-lg-3 col-md-3 form-group">
                                                        <button type="submit" name="add_new_Submit" class="btn btn-block btn-danger">Add New</button>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-lg-3 col-md-3 form-group">
                                                        <button type="submit" class="btn btn-block btn-success">Update Now</button>
                                                    </div>
                                                <?php } ?>
                                                <div class="col-lg-3 col-md-3 form-group">
                                                    <button type="reset" class="btn btn-block btn-warning">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name </th>
                                            <th>Code </th>
                                            <th style="width:3%; text-align: center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name </th>
                                            <th>Code </th>
                                            <th style="width:3%; text-align: center;">Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        while ($row = mysqli_fetch_assoc($result)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?= $row['bra_name']; ?></td>
                                                <td><?= $row['bra_code']; ?></td>
                                                <td>
                                                    <a href="branch_list.php?bra_id=<?php echo base64_encode($row['bra_id']); ?>">Edit</a>
                                                </td>   
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>





        <?php include_once './control-sidebar.php'; ?>





        <!-- /.content-wrapper -->
        <?php include_once './footer.php'; ?>

    </div>
    <!-- ./wrapper -->
</body>
</html>
