<?php

include_once '../../conn.php';
include_once '../../inc/functions.php';

//Fetching Values from URL
if (isset($_POST['bra_id'])) {
    $bra_id = $_POST['bra_id'];
} else {
    $bra_id = 0;
}
if (isset($_POST['bra_name'])) {
    $bra_name = $_POST['bra_name'];
} else {
    $bra_name = 0;
}
if (isset($_POST['bra_code'])) {
    $bra_code = $_POST['bra_code'];
} else {
    $bra_code = '';
}

$action = $_POST['action'];
$today = date('Y-m-d');

if ($action == 'register') {

    if ($bra_name != '') {

        $sqlcheck = "SELECT * FROM branch WHERE bra_name='" . $bra_name . "'";
        $result = mysqli_query($conn, $sqlcheck);

        if (mysqli_num_rows($result) > 0) {
            header('Location: ../branch_list.php?error=' . base64_encode(8));
        } else {
            $sql = "INSERT INTO `branch` ( `bra_name`, `bra_code`) VALUES ( '" . $bra_name . "', '" . $bra_name . "')";

            if (mysqli_query($conn, $sql)) {

                header('Location: ../branch_list.php?error=' . base64_encode(4));
            } else {
                header('Location: ../branch_list.php?error=' . base64_encode(3));
            }
        }
    } else {
        header('Location: ../branch_list.php?error=' . base64_encode(3));
    }
}

if ($action == 'update' && $bra_id > 0) {


    $sql = "update branch set bra_name='" . $bra_name . "',`bra_code`='" . $bra_code . "' where bra_id='" . $bra_id . "'";

    if (mysqli_query($conn, $sql)) {

        header('Location: ../branch_list.php?bra_id=' . base64_encode($bra_id) . '&error=' . base64_encode(1));
    } else {
        header('Location: ../branch_list.php?bra_id=' . base64_encode($bra_id) . '&error=' . base64_encode(3));
    }
}


 
